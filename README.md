<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" height = 20 /></a> <a href="https://liberapay.com/benz0li/donate"><img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate using Liberapay" height="20"></a>

# Best practice
[This repository](https://gitlab.com/b-data/r/best-practice) explains some
"best practices" in plain data analysis using RStudio and proposes a structure for folders
as well as R scripts.

:exclamation: **Do not** use this project as basis of your own ones.

*  There is a template for this purpose.  
   :information_source: See
[repository](https://gitlab.com/b-data/r/template)
and [instructions](https://gitlab.com/b-data/r/template/-/wikis/home).
*  There is a template using [packrat](https://rstudio.github.io/packrat/), too.  
   :information_source: See
[repository](https://gitlab.com/b-data/r/packrat-template)
and [instructions](https://gitlab.com/b-data/r/packrat-template/-/wikis/home).

# Set-up
## How to clone this project

1.  **Copy the project's URL:** `git@gitlab.com:b-data/r/best-practice.git`  
2.  **In RStudio go to Menu "File" > "New Project" > "Version Control"**  
    ![Version Control](screenshots/rssp_new-project_version-control.png)
3.  **Select "Git"**  
    ![Git](screenshots/rssp_new-project_version-control_git.png)
4.  **Paste the copied URL into field "Repository URL:"**  
    ![Clone Git Repository](screenshots/rssp_new-project_version-control_git_clone-repository.png)
5.  **Click "Create project"**

# Files
## Default
When checking out an empty Git repository, RStudio creates by default
files **".gitignore"** and **"\<project-name\>.Rproj"** (in this case
"best-practice.Rproj").

### About ".gitignore"
A **".gitignore"** file specifies intentionally untracked files that Git should
ignore. Files already tracked by Git are not affected.  

:point_right: see [https://git-scm.com/docs/gitignore#_description](https://git-scm.com/docs/gitignore#_description)
for more information.

**Default content**
```
.Rproj.user
.Rhistory
.RData
.Ruserdata
```

:point_right: RStudio automatically excludes files ".Rproj.user"
(project-specific temporary files), ".Rhistory" (loaded into the RStudio
History pane), ".RData" (saved workspace) and .Ruserdata (?).

### About "\<project-name\>.Rproj"
The **"\<project-name\>.Rproj"** file is a simple text file holding some of the
project's options (see Menu "Tools" > "Project Options...").

**Default content (RStudio; Linux)**
```
Version: 1.0

RestoreWorkspace: Default
SaveWorkspace: Default
AlwaysSaveHistory: Default

EnableCodeIndexing: Yes
UseSpacesForTab: Yes
NumSpacesForTab: 2
Encoding: UTF-8

RnwWeave: Sweave
LaTeX: pdfLaTeX

AutoAppendNewline: Yes
```

:point_right: If you click on an ".Rproj" file it does not show its source but
starts a session with the options defined therein.

## ".Rprofile" and ".keep"
### About ".Rprofile"
File ".Rprofile" is used to customise the R session at startup. According to
[R for Data Science (r4ds)](https://r4ds.had.co.nz/workflow-scripts.html), you
should

> [...] always start your script with the packages that you need. That way, if
> you share your code with others, they can easily see what packages they need
> to install. Note, however, that you should never include `install.packages()`
> or `setwd()` in a script that you share. It’s very antisocial to change
> settings on someone else’s computer!

To prevent the installation of packages in the user's library, ".Rprofile" sets
up a project specific library and adds it to the top of the search paths for
packages.

**Default content**
```R
local({
  if (file.exists("~/.Rprofile")) {
    source("~/.Rprofile")
  }
  
  lib <- file.path("lib", R.version$platform, substr(getRversion(), 1, 3))
  if (!file.exists(lib)) {
    dir.create(lib, recursive = TRUE)
  }
  .libPaths(c(lib, Sys.getenv("R_LIBS_USER")))
})
```

**Result (on most Linuxes)**
```R
> .libPaths()
[1] "/home/<user>/projects/<project-name>/lib/x86_64-pc-linux-gnu/3.6"
[2] "/home/<user>/R/x86_64-pc-linux-gnu-library/3.6"                 
[3] "/usr/local/lib/R/site-library"                                                    
[4] "/usr/lib/R/site-library"                                                          
[5] "/usr/lib/R/library"
```

:information_source: [packrat-template](https://gitlab.com/b-data/r/packrat-template)
uses a [different ".Rprofile"](https://gitlab.com/b-data/r/packrat-template/-/blob/main/.Rprofile)
and requires you to [install the development utilities](https://gitlab.com/b-data/r/packrat-template/-/wikis/home#install)
to build R packages from source.

### About ".keep"
File **".keep"** is an empty, hidden file used to push empty folders, because
these are ignored by Git by default.

The easiest way to create file ".keep" in the current directory is by executing
`touch .keep` in RStudio **Terminal** (on Linux).

## Best practice
Use https://www.gitignore.io/api/visualstudiocode,macos,linux,r,windows and
extend the **".gitignore"** file with the lines shown below:

```
config.yml
/*.html
/*.pdf

**/input/
**/output/
lib/
secret/**
temp/

!.keep
```
_→ In case of this specific project, there are the lines `/*.html`and `/*.pdf`
to exclude knitted HTML- and PDF-previews in the working directory._

**Explanations**

*  Line `config.yml`, because you **only want** to provide a sample
   configuration (`config.yml.sample`).
*  Lines `**/input/` and `**/output/` because you **do not** want to include
   any data or results stored in these [sub]folders.
*  Line `lib/`, because you **do not** want the project specific library being
   part of your Git repository.  
   :information_source: [packrat-template](https://gitlab.com/b-data/r/packrat-template):
   `packrat/lib*/`
*  Line `secret/**`, because you **must never** push secret or sensitive
   information to a remote Git repository.  
    → The trailing `/**` matches everything inside folder `secret`.
*  Line `temp/`, because there is no intention to push temporary data, either.  
    → The trailing `/` is used to only find a match within directories.
*  Line `!.keep`, because we always want to include this hidden file.  
    → i.e. for pushing empty folder "secret".

:point_right: see [https://git-scm.com/docs/gitignore#_pattern_format](https://git-scm.com/docs/gitignore#_pattern_format)
for a comprehensive explanation on the pattern format.

# Folders
A project conducting data analysis should always contain at least the following
folders:

1.  Folder `references` with websites that you have viewed at a certain time or
    documents that are not accessible to everyone.
2.  Folder `secret`, where files with credentials or other sensitive information
    is located.  
    → Preferrably in YAML format to extend your basic configuration.
3.  Folder `temp`, where you can save temporary files or anything else.  
    → Create and delete it as you like.

This folder structure is only suitable for plain data analysis, not for writing
reports, developing R packages, etc.  
_→ In case of this specific project, there is the additional folder
`screenshots`, for example._

:point_right: This suggestion will give you an idea on how to build the folder
structures for those.

## Best practice
*  Folder `references` and its content is pushed to the remote Git repository,
   because everyone contributing to the project should have access to it.
*  Only the empty folder `secret` is pushed to the remote Git repository,
   because it contains **your personal** logins, password or other sensitive
   information.  
    :exclamation: **Never share** your personal logins and passwords.
    **Everyone** should use **their own**.  
    :point_right: **You do not want someone else messing things up in your
    name.**
*  Folder `temp` is not pushed to the remote Git repository at all.

# R scripts
For most data analyses it is suggested to use the following structure of R
scripts:

*  00-Main.R
*  01-LoadPkgs.R
*  02-ReadConfig.R
*  03-Init.R
*  04-LoadFunc.R
*  05-ImportData.R
*  06-TidyData.R
*  07-TransformData.R
*  08-VisualiseData.R
*  09-ModelData.R
*  10-SaveOutput.R

:point_right: You can run the code of this project, which is linked to path
`.` (working directory), by sourcing file `00-Main.R`.

## Best practice
### 00-Main.R
This file is the "control centre" for the data analysis. It is used to

1.  **declare the necessary packages**  
    and
1.  **`source` all other files involved.**

:point_right: You should **not** "hard code" configuration parameters, but
provide a working `config.yml.sample` instead.

### 01-LoadPkgs.R
This file not only loads the necessary packages but also installs any missing R
package declared in file `00-Main.R`.

---

:information_source: [packrat-template](https://gitlab.com/b-data/r/packrat-template)
uses `packrat::restore()` to install R packages.

Please follow the workflow below, so that packrat is aware of the packages to
install for your project:

> [Add] install packages however you normally do:
> 
> ```R
> install.packages(pkgs)
> ```
> 
> Take a snapshot to save the changes in Packrat:
> 
> ```R
> packrat::snapshot()
> ```
> 
> For packages that were added or modified, packrat attempts to go find the
> uncompiled _source package_ from CRAN, BioConductor, or GitHub (caveat: only
> for packages that were installed using _devtools_ version 1.4 or later), and
> save them in the `packrat/src` project subdirectory. It also records metadata
> about each package in the `packrat.lock` file.
> 
> Because we save source packages for all of your dependencies, packrat makes
> your project more **reproducible**. When someone else wants to run your
> project – even if that someone else is you, years in the future, dusting off
> some old backups – they won’t need to try to figure out what versions of what
> packages you were running, and where you got them.

— [Packrat: Reproducible package management for R](https://rstudio.github.io/packrat/walkthrough.html#adding-removing-and-updating-packages)

---

We purposely do not call `library(config)` but rather qualify calls with the
package name (e.g. `config::get()`). This is to avoid conflicts between config
functions and functions in the base R package (specifically, `get` and `merge`).

### 02-ReadConfig.R
Copies "config.yml.sample" to "config.yml", if the latter is inexistent, and
reads the configuration.

#### config.yml.sample
File `config.yml.sample` contains a sample configuration with all necessary
parameters to run the project.

:point_right: See [https://github.com/rstudio/config](https://github.com/rstudio/config)
for usage, configurations, defaults and inheritance.

### 03-Init.R
File `03-Init.R` serves the following purposes:

1.  **Delete all objects**  
    → Some consider this bad practice, others prefer a clean workspace to
    start.
2.  **Define paths for input and output**  
    → input: :exclamation: This ensures, that R scripts and data are always
    stored seperately.  
    → output: For each user executing this analysis, a separate subdirectory
       is created.
3.  **(Delete all files and folders in output directory)**  
    → Commented out for safety reasons  
    :exclamation: `unlink(..., recursive = TRUE)` in R is like `rm -r ...` in
    Linux.

### 04-LoadFunc.R
The idea is that you define any custom functions here, that are needed for the
analysis.

### 05-ImportData.R to 09-ModelData.R
According to [R for Data Science (r4ds)](https://r4ds.had.co.nz/introduction.html#what-you-will-learn)  
(Do not forget to purge obsolete variables or do garbage collection.)

### 10-SaveOutput.R
Save your plots, tables, etc. in a subfolder `%Y%m%d` in your personal
output directory.

:point_right: If you run for-loops on graphics output, it might be more
convenient to use a temporary variable in `08-VisualiseData.R` and save it right
away.

## Ideal world
In an ideal world and after `sourcing` 00-Main.R,

1.  you should be able to run from any to the last R script and still produce
    the same results.
2.  each of the R scripts produces the same result for the variables defined in
    it, even if it is run several times in a row.  
    → An implication of 1.
